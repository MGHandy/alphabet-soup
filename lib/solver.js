var Solver = (function() {
	const util = require("../lib/util.js");

	var list_positions	= null;
	var list_words = null;

	var grid_rows = 0;
	var grid_columns = 0;

	var result_flag = false;

	/**
	* _load
	*
	* this function sets the necessary data to solve
	* the puzzle.
	*
	* @param positions		- list of character positions from the grid
	* @param words			- list of words to search for
	*/
	function _load(positions, words) {
		list_positions = positions;
		list_words = words;
	}

	/**
	 * _find
	 *
	 * this function can be used to search for a single word
	 *
	 * @param word		- the word to search for
	 */
	function _find(word) {
		return _search(word, 0, null, null);
	}
	
	/**
	 * _solve
	 *
	 * this function is used to solve for all words in the given list.
	 */
	function _solve() {
		list_words.forEach((word) => {

			var result_word = word.replace(/\s/g, "");
			var result = _search(result_word, 0, null, null);
			if (result == null) {
				console.log(word + " NOT FOUND");
			} else {
				console.log(word + " " + result.start.row + ":" + result.start.column + " " + result.end.row + ":" + result.end.column);
			}
		});
	}

	/**
	 * _search
	 *
	 * this is a private recursive function which performs the search for
	 * and individual word.
	 *
	 * @param word			- the word to search for
	 * @param index			- the current character position in the word
	 * @param previous		- the previous position to match distance/directions
	 * @param direction		- the current direction of search, using compass values
	 */
	function _search(word, index, previous, direction) {
		var word_search = null;
		var word_data = {
			"word"	: word,
			"start"	: "null",
			"end"	: "null"
		};

		const character = word.charAt(index);

		if (index == word.length) {
			previous.end = true;
			return previous;
		}
	
		// iterate all positions for current character
		const next_positions = list_positions[character];
		for (var i = 0; i < next_positions.length; i++) {
		
			const next = next_positions[i];

			/**
			 * if the previous position is null it means the search is at the beginning
			 * and have not yet save a character position
			 *
			 * in this case the next position is the first letter of the word
			 * set as the start position and advance
			 */
			if (previous == null) {
				var next_search = _search(word, index + 1, next, direction);
				if (next_search != null) {
					word_data.start = next;
					word_data.end = next_search;
					word_search = word_data;
					break;
				}
			} else {

				var next_distance = util.getDistance(previous, next);
				var next_direction = util.getDirection(previous, next);
				
				if (next_distance == 1) {
					
					if (direction == null) {
						word_search = _search(word, index + 1, next, next_direction);
					}

					if (direction != null && direction == next_direction) {
						word_search = _search(word, index + 1, next, direction);
					}

					if (word_search != null && word_search.end) {
						break;
					}
				}
			}

		}
		
		return word_search;
	}

	return {
		load:		_load,
		find:		_find,
		solve:		_solve,
		positions:	list_positions,
		words:		list_words
	};
})();

module.exports = Solver;
